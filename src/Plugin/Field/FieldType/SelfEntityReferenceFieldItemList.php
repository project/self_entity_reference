<?php

declare(strict_types=1);

namespace Drupal\self_entity_reference\Plugin\Field\FieldType;

use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Defines a item list class for dynamic entity reference fields.
 */
class SelfEntityReferenceFieldItemList extends EntityReferenceFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function get($index) {
    $this->computeSelfReferenceList();
    return parent::get($index);
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator() {
    $this->computeSelfReferenceList();
    return parent::getIterator();
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $this->computeSelfReferenceList();
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue($include_computed = FALSE) {
    $this->computeSelfReferenceList();
    return parent::getValue($include_computed);
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities(): array {
    return [0 => $this->getEntity()];
  }

  /**
   * Compute the self entity reference list.
   */
  protected function computeSelfReferenceList(): void {
    if (!isset($this->list[0]) || $this->list[0]->isEmpty()) {
      $this->list[0] = $this->createItem(0, [
        'target_id' => $this->getEntity()->id(),
        'target_type' => $this->getEntity()->getEntityTypeId(),
      ]);
    }
  }

}
