<?php

declare(strict_types=1);

namespace Drupal\self_entity_reference;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\self_entity_reference\Plugin\Field\FieldType\SelfEntityReferenceFieldItemList;

/**
 * The class EntityTypeInfo.
 *
 * @internal
 */
class EntityTypeInfo {

  use StringTranslationTrait;

  /**
   * Adds the computed self entity reference field as a base field to entities.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type.
   *
   * @return array
   *   Updated base field info with computed self entity reference field.
   */
  public function entityBaseFieldInfo(EntityTypeInterface $entityType): array {
    $fields = [];
    if (in_array($entityType->id(), $this->excludeEntityTypes(), TRUE)) {
      return $fields;
    }

    $fields['self_entity_reference'] = BaseFieldDefinition::create('entity_reference')
      ->setName('self_entity_reference')
      ->setTargetEntityTypeId($entityType->id())
      ->setSetting('target_type', $entityType->id())
      ->setLabel($this->t('Self Entity Reference'))
      ->setTranslatable(FALSE)
      ->setComputed(TRUE)
      ->setClass(SelfEntityReferenceFieldItemList::class)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Entity types (node, taxonomy_term), that should not have a self reference.
   *
   * @return array
   *   Array of entity type machine names to exclude.
   */
  protected function excludeEntityTypes(): array {
    // @todo This should be configurable.
    return [];
  }

}
